# Lynx

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) 

## Api

[List all applications: /api/application](https://lynx-angular.herokuapp.com/api/application)

This is endpoint 1, GET parameters `id` and `date`(YYYY-MM-DD format) could be used

##

[Generate PDF: /api/pdf/application](https://lynx-angular.herokuapp.com/api/pdf/application)  

This is endpoint 2, GET parameter `id` should be used


## Deploy

* Clone repository   

`git clone https://gitlab.com/RedLobster/lynx-angular.git`

*  Switch folder   

`cd lynx-angular.git`

*  Create .env file  

`touch .env`

*  Set content of .env file to: 

`MONGODB_URI=mongodb://lynx:RedLobster87@ds139768.mlab.com:39768/heroku_11htjv0d`

 `DBNAME=heroku_11htjv0d`
 
*  Install packages

`npm install`

*  Build

`ng build`

*  Run

`node server.js`
