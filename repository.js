const ObjectId = require('mongodb').ObjectID;

module.exports = function(storage) {

  return {
    save: function(application, callback){
      storage.client.connect(function(err) {
        const db = storage.client.db(storage.dbName);
        const collection = db.collection('applications');
        collection.insertOne(application, function(err, result) {
          storage.client.close();
          callback(result ? result.insertedId : result);
        });
      });
    },

    all: function(callback){
      storage.client.connect(function(err) {
        const db = storage.client.db(storage.dbName);
        const collection = db.collection('applications');
        collection.find({}).toArray(function(err, docs) {
          storage.client.close();
          callback(docs && docs.length ? docs : []);
        });
      });
    },

    forDate: function(date, callback) {
      storage.client.connect(function(err) {
        const db = storage.client.db(storage.dbName);
        const collection = db.collection('applications');
        date.setDate(date.getDate());
        const start = date.toISOString().split('T')[0];
        date.setDate(date.getDate() + 1);
        const end = date.toISOString().split('T')[0];
        const filter = {
          "date": {
            "$gte": new Date(start + "T00:00:00.000Z"),
            "$lt": new Date(end + "T00:00:00.000Z")
          }
        };
        collection.find(filter).toArray(function (err, docs) {
          storage.client.close();
          callback(docs && docs.length ? docs : []);
        });
      });
    },

    one: function(id, callback){
      if (ObjectId.isValid(id)) {
        storage.client.connect(function(err) {
          const db = storage.client.db(storage.dbName);
          const collection = db.collection('applications');
          collection.findOne({"_id": ObjectId(id)}, function(err, obj) {
            storage.client.close();
            callback(obj);
          });
        });
      } else {
        callback(null);
      }
    }
  };
};
