const express = require('express');
const path = require('path');
const app = express();
const pdf = require('express-pdf');
const storage = require('./db')(app);
const pdfDataProvider = require('./pdfDataProvider')();
const repository = require('./repository')(storage);

app.use(express.static(__dirname + '/dist/lynx'));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(pdf);

require('./api_routes')(app, repository, pdfDataProvider);

app.get('/*', function(req, res) {
      res.sendFile(path.join(__dirname + '/dist/lynx/index.html'));
  });
app.listen(process.env.PORT || 8080);
