const pug = require('pug');
const moment = require('moment');

module.exports = function() {
  const compiledFunction = pug.compileFile('template.pug');
  const filename = 'generated.pdf';

  return {
    generateOne: function (doc) {
      const html = compiledFunction({doc: doc, moment: moment});
      return {
        filename: filename,
        htmlContent: '<html><body>'+ html +'</body></html>',
      }
    },

    generateMany: function (docs) {
      let html = '';
      for (const doc of docs) {
        html += compiledFunction({doc: doc, moment: moment});
      }

      return{
        filename: filename,
        htmlContent: '<html><body>'+ html +'</body></html>',
      };
    }
  };
};
