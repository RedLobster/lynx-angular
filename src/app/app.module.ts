import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { WINDOW_PROVIDERS } from './service/window.provider';
import { ApiService } from './service/api.service';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {InfoFactory} from './service/info.factory';
import {ApplicationFactory} from './service/application.factory';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    WINDOW_PROVIDERS,
    ApiService,
    InfoFactory,
    ApplicationFactory
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
