import { Component, OnInit } from '@angular/core';
import { QuestionnaireModel, QuestionModel } from '../model/questionnaireModel';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent  {


  application: QuestionnaireModel;
  lastAddedId: string;


  constructor(private apiService: ApiService) {
    this.resetForm();
  }


  onSubmit() {
    const self = this;
    this.apiService.add(this.application).subscribe((doc) => { console.log(doc); self.lastAddedId = doc._id; });

    this.resetForm();
  }


  private resetForm() {
    this.lastAddedId = null;
    const question1 = new QuestionModel('Question 1', false);
    const question2 = new QuestionModel('Question 2', false);
    const question3 = new QuestionModel('Question 3', false);
    const question4 = new QuestionModel('Question 4', false);
    const question5 = new QuestionModel('Question 5', true);
    const question6 = new QuestionModel('Question 6', false, null, question5);

    this.application = new QuestionnaireModel(
      [
        question1,
        question2,
        question3,
        question4,
        question5,
        question6
      ]
    );
  }
}
