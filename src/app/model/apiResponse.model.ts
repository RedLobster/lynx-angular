import {InfoModel} from './applicationModel';

export class ApiResponseModel {
  constructor(
    public data: InfoModel[],
    public date: string,
    public _id: string,
  ) {  }
}
