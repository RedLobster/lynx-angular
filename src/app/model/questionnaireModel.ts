export class QuestionModel {
  constructor(
    public name: string,
    public isBool: boolean,
    public answer?: any,
    public dependsOn?: QuestionModel,
  ) {  }

  isFilled(): boolean {
    return this.isBool ? this.answer as boolean : this.answer && this.answer.length > 0;
  }
}


export class QuestionnaireModel {
  constructor(
    public questions: QuestionModel[]
  ) {  }
}
