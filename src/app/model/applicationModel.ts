export class InfoModel {
  constructor(
    public question: string,
    public answer: string
  ) {  }
}


export class ApplicationModel {
  constructor(
    public data: InfoModel[]
  ) {  }
}
