import { Injectable, Inject } from '@angular/core';
import {ApplicationModel, InfoModel} from '../model/applicationModel';
import {QuestionnaireModel} from '../model/questionnaireModel';
import {InfoFactory} from './info.factory';

@Injectable()
export class ApplicationFactory {

  constructor(
    @Inject(InfoFactory) private infoFactory: InfoFactory
  ) {}

  fromQuestionnaire(questionnaire: QuestionnaireModel): ApplicationModel {
    const data = new Array<InfoModel>();
    for (const question of questionnaire.questions) {
      if (question.isFilled()) {
        data.push(new InfoModel(question.name, question.answer));
      }
    }

    return new ApplicationModel(data);
  }
}
