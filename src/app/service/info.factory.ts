import { Injectable, Inject } from '@angular/core';
import {InfoModel} from '../model/applicationModel';
import {QuestionModel} from '../model/questionnaireModel';

@Injectable()
export class InfoFactory {

  fromQuestion(question: QuestionModel): InfoModel {
    return new InfoModel(question.name, question.answer);
  }
}
