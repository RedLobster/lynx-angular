import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { WINDOW } from './window.provider';

import { ApiResponseModel } from '../model/apiResponse.model';
import { QuestionnaireModel } from '../model/questionnaireModel';
import { ApplicationFactory } from './application.factory';
import { ApplicationModel, InfoModel } from '../model/applicationModel';


const URI = '/api/application';


@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient,
    @Inject(ApplicationFactory) private applicationFactory: ApplicationFactory,
    @Inject(WINDOW) private window: Window
  ) {}


  public add(questionnaire: QuestionnaireModel): Observable<ApiResponseModel> {
    const application = this.applicationFactory.fromQuestionnaire(questionnaire);
    const request = this.http.post<ApiResponseModel>(
      this.getUrl(URI),
      JSON.stringify(application),
      {headers: {'Content-Type': 'application/json'}}
    );

    return request;
  }

  private getUrl(path) {
    let url = window.location.protocol + '//' + window.location.hostname;
    if (window.location.port) {
      url += ':' + window.location.port;
    }
    url += path;

    return url;
  }
}
