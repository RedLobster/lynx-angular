module.exports = function(app) {

  if (app.get('env') !== 'production'){
    require('dotenv').config();
  }

  const MongoClient = require('mongodb').MongoClient;
  const url = process.env.MONGODB_URI;
  const client = new MongoClient(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

  return {
    client: client,
    dbName: process.env.DBNAME
  };
};
