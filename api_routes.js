module.exports = function(app, repository, pdfDataProvider) {
  const endpoint1 = '/api/application';
  const endpoint2 = '/api/pdf/application';

  app
    .get(endpoint1, (req, res) => {
      if (req.query.id !== undefined && req.query.id) {
        repository.one(req.query.id, function (doc) {
          res.status(doc ? 200 : 404).send(doc);
        });
      } else if (req.query.date !== undefined && req.query.date && Date.parse(req.query.date)) {
        repository.forDate(new Date(req.query.date), function (docs) {
          res.status(200).send(docs);
        });
      } else {
        repository.all(function (docs) {
          res.status(200).send(docs);
        });
      }
    })

    .post(endpoint1, (req, res) => {
      const application = req.body;
      application.date = new Date();
      repository.save(application, function (id) {
        if (id) {
          application._id = id;
          res.status(201).send(application);
        } else {
          res.status(500).send({error: 'Failed to save'});
        }
      });
    })

    .get(endpoint2, (req, res) => {
      if (req.query.id !== undefined && req.query.id) {
        repository.one(req.query.id, function (doc) {
          if (doc) {
            res.pdfFromHTML(pdfDataProvider.generateOne(doc));
          } else {
            res.status(404).send('');
          }
        });
      } else {
        repository.all(function (docs) {
          res.pdfFromHTML(pdfDataProvider.generateMany(docs));
        });
      }
    })
};
